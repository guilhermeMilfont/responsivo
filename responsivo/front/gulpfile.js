// Inicialização e rquisições
var gulp = require('gulp');
var runSequence = require('run-sequence');
var del = require('del');
var browserSync = require('browser-sync').create();
var compass = require('gulp-compass');


// Tarefas

gulp.task('compass', function() {
  gulp.src('app/scss/**/*.scss')
    .pipe(compass({
      config_file: 'app/config.rb',
      css: 'app/css',
      sass: 'app/scss'
    }))
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.reload({
		stream: true
	}))
});

// Task para o Browser Sync
gulp.task('browserSync',function(){
	browserSync.init({
		server: {
			baseDir: 'app'
		},
	});
});

// task para limpar dist
gulp.task('clean:dist', function() {
	return del.sync('dist');
})

// Task para watch
gulp.task('watch', ['browserSync','compass'] ,function(){
	gulp.watch('app/scss/**/*.scss',['compass']);
	gulp.watch('app/*.html', browserSync.reload);
	gulp.watch('app/js/**/*.js', browserSync.reload);
});

// Task default
gulp.task('default', function(callback) {
	runSequence('clean:dist', ['compass','browserSync','watch'],
        callback
    )
})